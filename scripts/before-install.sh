if [ -d /opt/demonsaw ]; then
	if [ -f /opt/demonsaw/demonsaw.xml ]; then
		echo "Previous demonsaw.xml found, backing up for upgrade..."
		if [ ! -d ~/.config/ ]; then
			mkdir ~/.config
		fi
		cp /opt/demonsaw/demonsaw.xml ~/.config/demonsaw.xml
	fi
fi
