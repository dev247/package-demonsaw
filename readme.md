# Demonsaw Packaging Scripts

The included scripts are for automating .deb package builds using [fpm](https://github.com/jordansissel/fpm/). Please see the fpm page for instructions on installing it.

This repository will allow you to quickly build .deb packages for 32 and 64 bit, ubuntu and debian; regardless of whether you are on ubuntu or debian for your host operating system. 

## How it works
The package.py gives you functions to --clean your build environment, --verify the build environment, and to --build the packages. The script assumes that you have downloaded all the .tar.gz files into the archives/ folder for each linux build available, leaving their default names.

Some files that don't usually change will be copied from the install/ folder, the rest of the files will be extracted from the archive of the version being built. package.py is basically performing a manual install of demonsaw and using fpm to package them up into a .deb.

## Options

`sudo ./package.py --clean`
This will fully remove all installed files and remove downloaded archives.

`sudo ./package.py --install`
This will verify the current packaging folders and copy the static install files into the operating system.

`sudo ./package.py --verify`
This is a way to manually trigger and make sure all files needed for a build are in existence.

`sudo ./package.py --build`
The meat. This runs the build for all demonsaw versions.