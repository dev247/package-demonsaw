#!/bin/bash

target=$1
architecture=$2
version=$3
build=$4
output=$5
beforeinstall="/home/fed/workspace/package-demonsaw/scripts/before-install-fed.sh"
afterinstall="/home/fed/workspace/package-demonsaw/scripts/after-install.sh"
afterupgrade="/home/fed/workspace/package-demonsaw/scripts/after-upgrade.sh"

fpm -s dir -t ${target} -a ${architecture} -n "demonsaw" -v ${version} --vendor "Demonsaw LLC" -m "dekka@demonsaw.com" --description "Demonsaw is a secure and anonymous information sharing application that makes security simple and gives you back control of your data. Chat, message, and share files without fear or consequence." --url "https://demonsaw.com/" --package ${output} --depends "gstreamer-plugins-base = 0.10.36-13.fc23" --before-install ${beforeinstall} --after-install ${afterinstall} --after-upgrade ${afterupgrade} --force /opt/demonsaw /usr/share/applications/demonsaw.desktop
