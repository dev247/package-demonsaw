#!/usr/bin/python2.7

# Script for packaging and cleanup
import shutil, os, sys
import tarfile, getopt
import subprocess as sub

## File Names (in case they change in the future)
fed32 = "demonsaw_debian_32.tar.gz"
fed64 = "demonsaw_debian_64.tar.gz"
verno = "2.7.1"

## Other variables
currdir = os.getcwd()
buildscript = os.path.join(currdir, "buildfed.sh")
instfold = os.path.join(currdir, "install")
builddir = os.path.join(currdir, "builds")
archives = os.path.join(currdir, "archives")
foldstruct = [builddir, instfold, archives]

def Verify():
	## Verfiy file structure for install/build
	folderrs = 0
	print "\tVerifying our folders exist..."
	for fold in foldstruct:
		try:
			os.makedirs(fold)
		except OSError:
			if not os.path.isdir(fold):
				raise

	print "\tVerifying our files have been downloaded..."
	if not os.path.isfile(os.path.join(archives,fed32)):
		print "\t ERROR: %s is missing." % fed32
		folderrs += 1
	if not os.path.isfile(os.path.join(archives,fed64)):
		print "\t ERROR: %s is missing." % fed64
		folderrs += 1

	if folderrs > 0:
		sys.exit("\tToo many errors to continue, download missing files and retry.")
	else:
		print "\tPackaging structure looks to be ready."


def Clean():
	## We're going to remove previous install files
	print "Cleaning up demonsaw install..."
	try:
		shutil.rmtree("/opt/demonsaw")
	except:
		print "\tUnable to remove /opt/demonsaw/"
	try:
		os.remove("~/.config/demonsaw.xml")
	except:
		print "\tWARN: Unable to remove ~/.config/demonsaw.xml"
	try:
		os.remove("/usr/share/applications/demonsaw.desktop")
	except:
		print "\t WARN: demonsaw.desktop doesn't exist or could not be removed."

	print "Clean-up complete."

def Install():
	## Extract only the necessary files from the specified tar.gz to install directory
	## including the demonsaw.desktop file that is prepackaged with this script.
	## demonsaw.desktop, demonsaw.xml, demonsaw.ico
	print "Running --verify to ensure we're ready."
	Verify()
	print "Running install."
	print "\tCreating demonsaw folder..."
	try:
		os.makedirs('/opt/demonsaw')
	except OSError:
		if not os.path.isdir('/opt/demonsaw'):
			raise
	print "\tInstalling static files..."
	shutil.copy(os.path.join(instfold, "demonsaw.xml"), "/opt/demonsaw/demonsaw.xml")
	shutil.copy(os.path.join(instfold, "demonsaw.ico"), "/opt/demonsaw/demonsaw.ico")
	shutil.copy(os.path.join(instfold, "demonsaw.desktop"), "/usr/share/applications/demonsaw.desktop")
	shutil.copy(os.path.join(instfold, "start"), "/opt/demonsaw/start")
	print "Install complete."

def Setup(build):
	## This function takes full archive path as variable and extracts needed files to /opt/demonsaw
	os.chdir("/opt/demonsaw/")
	keepers = ["demonsaw", "demonsaw_cli", "version.txt", "readme.txt"]
	for keep in keepers:
		try:
			os.remove(keep)
		except:
			print "\tWARN: Couldn't remove previous file: %s" % keep

	if build == "fed32":
		archive = os.path.join(archives, fed32)
	elif build == "fed64":
		archive = os.path.join(archives, fed64)

	try:	
		tar = tarfile.open(archive, 'r:gz')
		for keep in keepers:
			try:
				tar.extract(keep)
			except:
				print "ERROR: Failed to extract " + keep
	except:
		sys.exit("ERROR: Failed to open archive.")

def Build():
	## We'll first make sure that the neccessary files are in place by running Clean() and 
	## Install() then extract the files we want from each tar.gz and run build command
	builds = ["fed32", "fed64"]
	for build in builds:
		if "32" in build:
			arch = "i386"
		else:
			arch = "amd64"
		fname = "demonsaw-%s-%s.rpm" % (verno, build)
		command = "sudo %s rpm %s %s %s %s" % (buildscript, arch, verno, build, os.path.join(builddir, fname))
		print "Prepping for builds..."
		os.chdir(builddir)
		print "\tSetting up %s build..." % build
		Setup(build)
		print "\tBuilding %s..." % build
		output = sub.call(['bash','-c', command])
	print "\nAll builds completed!"



def main(argv):
	print '''
***********************************************
	   Demonsaw Fed Packaging script          
***********************************************
'''
	try:
		opts, args = getopt.gnu_getopt(argv, 'b:c:i:v', ['build', 'clean', 'install', 'verify'])
	except getopt.GetoptError:
		print './package.py --clean | --install | --verify | --build'
		sys.exit(2)

	for opt, arg in opts:
		if opt in ('b', '--build'):
			Build()
		elif opt in ('c', '--clean'):
			Clean()
		elif opt in ('i', '--install'):
			Install()
		elif opt in ('v', '--verify'):
			Verify()

if __name__ == "__main__":
	main(sys.argv[1:])
